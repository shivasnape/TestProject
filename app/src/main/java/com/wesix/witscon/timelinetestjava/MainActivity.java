package com.wesix.witscon.timelinetestjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    RecyclerView recyclerView;
    List<DataList> dataList = new ArrayList<>();
    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);


        //mile stone recycler adapter
        recyclerAdapter = new RecyclerAdapter(this, dataList);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager2);
//        mileStoneRecyclerView.addItemDecoration(new SimpleDividerItemDecorator(this));
        recyclerView.setAdapter(recyclerAdapter);


        initView();


    }

    private void initView() {

        for (int i = 0; i < 4; i++) {

            DataList data = new DataList();

            data.setiProjectId(i);

            dataList.add(data);

        }

        recyclerAdapter.notifyDataSetChanged();


    }
}
