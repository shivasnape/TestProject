package com.wesix.witscon.timelinetestjava;

/**
 * Created by wesix on 27/3/18.
 */

public class DataList {

    int iSumofHours, iDrawingCount, iDocumentCount,iProjectId;
    String sProjectName,sDate;

    public DataList() {

    }

    public DataList(int hourSum, int drawingCount, int idocumentCount, int projectId, String projectName, String date) {

        this.iSumofHours = hourSum;
        this.iDrawingCount = drawingCount;
        this.iDocumentCount = idocumentCount;
        this.iProjectId = projectId;
        this.sProjectName = projectName;
        this.sDate = date;

    }


    public int getiSumofHours() {
        return iSumofHours;
    }

    public void setiSumofHours(int iSumofHours) {
        this.iSumofHours = iSumofHours;
    }

    public int getiProjectId() {
        return iProjectId;
    }

    public void setiProjectId(int iProjectId) {
        this.iProjectId = iProjectId;
    }

    public String getsProjectName() {
        return sProjectName;
    }

    public void setsProjectName(String sProjectName) {
        this.sProjectName = sProjectName;
    }

    public int getiDocumentCount() {
        return iDocumentCount;
    }

    public void setiDocumentCount(int iDocumentCount) {
        this.iDocumentCount = iDocumentCount;
    }

    public int getiDrawingCount() {
        return iDrawingCount;
    }

    public void setiDrawingCount(int iDrawingCount) {
        this.iDrawingCount = iDrawingCount;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }
}
